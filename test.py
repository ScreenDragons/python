def chooseCryption():
    cryption = ''
    while cryption !='e' and cryption !='d':
        print('Wil je een bericht encrypten of decrypten? (e of d)')
        cryption = input()

    return cryption
