print("Wil je een bericht encrypten of decrypten? (e of d)")
encryption = userinput()
output = " "

if encryption == "e":
    print("Type een bericht dat je graag zou willen encrypten:")
    message = userinput()
    print("Kies een sleutel:")
    key = userinput()

    ran = range(len(message))

    for i in ran:
        currentLetter = message[i]
        currentKeyLetter = key[i % len(key)]

        numberLetter = ord(currentLetter)
        numberKeyLetter = ord(currentKeyLetter)

        sumOfTwoLetters = numberLetter + numberKeyLetter

        newNumberLetter = sumOfTwoLetters % 128
        print(newNumberLetter)

        newLetter = chr(newNumberLetter)

        printText = currentLetter + "(" + str(numberLetter) + ") + "
        printText += currentKeyLetter + "(" + str(numberKeyLetter) + ") = "
        printText += newLetter + "(" + str(newNumberLetter) + ")"
        print(printText)
        output += newLetter


decryption = userinput()
output = ""

if decryption == "d":
    print("Welk bericht zou je graag willen decrypten?")
    message = input()
    print("Kies een sleutel")
    key = userinput()
    ran = range(len(message))

    for i in ran:
        currentLetter = message[i]
        currentKeyLetter = key[i % len(key)]

        numberLetter = ord(currentLetter)
        numberKeyLetter = ord(currentKeyLetter)

        difference = numberKeyLetter - numberLetter
        correctLetter = 128-(difference % 128)

        print(correctLetter)
        print(chr(correctLetter))
        output += chr(correctLetter)

        print(output)